<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mahasiswa;

class MahasiswaController extends Controller
{


    public function store(Request $request)
    {
        //dd($request);
        Mahasiswa::create($request->all());
        return redirect('/tampil_data');
    }

    public function index()
    {
        $mahasiswas = Mahasiswa::all();
        return view('mahasiswa.index', compact('mahasiswas'));
    }
}
